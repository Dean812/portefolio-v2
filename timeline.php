<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portefolio de Thibaut Descamps</title>
    <!-- fonts -->
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- fontawesome - icone -->
    <script src="https://kit.fontawesome.com/27a05ab3d9.js" crossorigin="anonymous"></script>
    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <header class="bg-rdark">                 <!--  HEADER   -->
        <div class="header_top">
            <section>
                <h4 class=" greentext font-weight-bold">Thibaut Descamps</h4>
                <p class="greytext">Développeur web Back-end</p>   
            </section>
            <aside class="language">
                <p class="greentext font-weight-bold">Français</p>
                <p class="greytext font-weight-bold">English</p>
            </aside>
        </div>
        <nav class="bg-dark">
            <div class="header_down">
                <a href="index.php"><i class="fas fa-home"></i><p class="hide">Accueil</p></a>
            </div>
            <div class="header_down">
                <a href="skills.php"><i class="fas fa-tools"></i><p class="hide">Compétences</p></a>
            </div>
            <div class="header_down">
                <a href="projects.php"><i class="fas fa-folder-open"></i><p class="hide">Projets</p></a> 
            </div>
            <div class="header_down">
                <a href="timeline.php"><i class="fas fa-hourglass-start actif"></i><p class="hide actif">Chronologie</p></a>
            </div>
            <div class="header_down">
                <a href="presentation.php"><i class="fas fa-user-circle"></i><p class="hide">Présentation</p></a>
            </div>
            <div class="header_down">
                <a href="contact.php"><i class="fas fa-envelope"></i><p class="hide">Contact</p></a>
            </div>
        </nav>
    </header>
    <main id="timeline">                  <!--      MAIN       -->
        <div class="title">
            <h2 class="font-weight-bold">Chronologie</h2>
        </div> 
    </main>

    <footer class="bg-transparent fixed-bottom text-center font-weight-bold"> <!-- FOOTER -->
       Tous Droits Réservés © 2020 - DESCAMPS Thibaut
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>   
</body>

</html>