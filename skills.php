<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portefolio de Thibaut Descamps</title>
    <!-- fonts -->
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- fontawesome - icone -->
    <script src="https://kit.fontawesome.com/27a05ab3d9.js" crossorigin="anonymous"></script>
    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <header class="bg-rdark">          <!--  HEADER   --> 
        <div class="header_top">
            <section>    <!-- Titre  - nom  -->
                <h4 class=" greentext font-weight-bold">Thibaut Descamps</h4>
                <p class="greytext">Développeur web Back-end</p>   
            </section>
            <section class="language"> <!-- langue  -->
                <p class="greentext font-weight-bold">Français</p>
                <p class="greytext font-weight-bold">English</p>
            </section>
        </div>
        <nav class="bg-dark">
            <div class="header_down">
                <a href="index.php"><i class="fas fa-home"></i><p class="hide ">Accueil</p></a>
            </div>
            <div class="header_down">
                <a href="skills.php"><i class="fas fa-tools actif"></i><p class="hide  actif">Compétences</p></a>
            </div>
            <div class="header_down">
                <a href="projects.php"><i class="fas fa-folder-open"></i><p class="hide">Projets</p></a> 
            </div>
            <div class="header_down">
                <a href="timeline.php"><i class="fas fa-hourglass-start"></i><p class="hide">Chronologie</p></a>
            </div>
            <div class="header_down">
                <a href="presentation.php"><i class="fas fa-user-circle"></i><p class="hide">Présentation</p></a>
            </div>
            <div class="header_down">
                <a href="contact.php"><i class="fas fa-envelope"></i><p class="hide">Contact</p></a>
            </div>
        </nav>
    </header>  
    
    <main id="skills">                  <!--      MAIN       -->
        <div class="title">
            <h2 class="font-weight-bold">Compétences</h2>
        </div> 
        <div class="container">
            <div class="training">              <!--        TRAINING         -->
                <div class="border border-secondary rounded mt-3">
                    <div class="card bg-transparent">                   <!--        2nd TRAINING         -->
                        <h5 class="card-header browntext font-weight-bold bg-light">2nd Formation : M2I Formation</h5>
                        <div class="card-body">
                            <p class="card-text">Cette seconde formation à permis d'approfondir et d'aggrandir mes connaissances et mon expérience dans le monde du web. Une expérience en stage était nécessaire en fin de formation pour l'obtention du diplôme</p>
                            <div class="d-inline bg-primary">
                                <section>
                                    <div class="orientaflex mt-2 bg-white bg-transparent w-100">
                                                                             
                                        <div class="card mt-2 desksize">
                                            <h5 class="card-header">OS & IDE</h5>
                                            <div class="card-body d-flex justify-content-around"> 
                                                <img src="img/logo/win10.png" alt="win10" title="Windows 10">
                                                <img src="img/logo/atom.png" alt="atom" title="Atom">
                                                <img src="img/logo/vsc.png" alt="vsc" title="Vsc">
                                                <img src="img/logo/phpstorm.png" alt="phpstorm" title="PhpStorm">
                                                <img src="img/logo/netbeans.png" alt="netbeans" title="NetBeans">
                                                <img src="img/logo/androidstudio.png" alt="androidstudio" title="Android Studio">
                                            </div>
                                        </div>
                                        <div class="card mt-2 desksize">
                                            <h5 class="card-header">LANGAGE</h5>
                                            <div class="card-body d-flex justify-content-around"> 
                                                <img src="img/logo/html-css.png" alt="front logos" Title="html5 & css3">
                                                <img src="img/logo/javascript.png" alt="javascript" Title="JavaScript">
                                                <img src="img/logo/php.png" alt="php" Title="Php 7">
                                                <img src="img/logo/java.png" alt="java" Title="Java 11">
                                            </div>
                                        </div>

                                        <div class="card mt-2 desksize">
                                            <h5 class="card-header">FRAMEWORK & CROSS PLATEFORM</h5>
                                            <div class="card-body d-flex justify-content-around">
                                                <img src="img/logo/symfony.png" alt="Symfony" title="Symfony 3.4">
                                                <img src="img/logo/spring.png" alt="spring" title="Spring"> 
                                                <img src="img/logo/ionic.png" alt="ionic" title="Ionic">    
                                            </div>
                                        </div>
                                        <div class="card mt-2 desksize">
                                            <h5 class="card-header">BDD</h5>
                                            <div class="card-body d-flex justify-content-around">
                                                <img src="img/logo/xampp.png" alt="xampp" title="Xampp">
                                                <img src="img/logo/mysql.png" alt="MySQL" title="MySQL">
                                                <img src="img/logo/mongodb.png" alt="MongoDB" title="MongoDB">
                                            </div>
                                        </div>
                                        <div class="card mt-2 w-100">
                                            <h5 class="card-header">STAGE</h5>
                                            <div class="card-body d-flex justify-content-around">
                                                <img src="img/logo/win7.png" alt="win7" Title="Win 7">
                                                <img src="img/logo/html-css.png" alt="front logos" Title="html5 & css3">
                                                <img src="img/logo/javascript.png" alt="javascript" Title="JavaScript">
                                                <img src="img/logo/wordpress.png" alt="wordpress" Title="Wordpress">
                                                <img src="img/logo/jquery.png" alt="Jquery" Title="Jquery et Ajax">
                                                <img src="img/logo/bootstrap.png" alt="bootstrap" Title="Bootstrap 4.5">
                                                <img src="img/logo/mysql.png" alt="mysql" Title="MySQL">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <aside>       
                                    <div class="embed-responsive embed-responsive-16by9 mt-3">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2532.09484198655!2d3.1511497156441117!3d50.60677518396965!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d7b048ecbc97%3A0xb7bd19de7bc0e94!2sM2i%20Formation!5e0!3m2!1sfr!2sfr!4v1589320782531!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                    </div>
                                </aside>     
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="border border-secondary rounded  mt-3">
                    <div class="card bg-transparent">                   <!--        1st TRAINING         -->
                        <h5 class="card-header browntext font-weight-bold bg-light">1ère Formation : 3W Academy</h5>
                        <div class="card-body">
                            <p class="card-text">Après mon retour en France, je me suis engagé à poursuivre des formations en Développement Web afin de débuter la carrière que j'ai toujours voulu faire, développeur web. </p>
                            <div class="d-flex justify-content-around border border-secondary rounded pt-2 pb-2 mt-2 bg-white">
                                <img src="img/logo/ubuntu.png" alt="ubuntu" Title="Ubuntu">
                                <img src="img/logo/atom.png" alt="Atom" Title="Atom">
                                <img src="img/logo/vsc.png" alt="vsc" title="Visual Studio Code">    
                                <img src="img/logo/html-css.png" alt="front logos" Title="html5 & css3">
                                <img src="img/logo/javascript.png" alt="javascript" Title="JavaScript">
                                <img src="img/logo/jquery.png" alt="Jquery" Title="Jquery 3.4.1 et Ajax">
                                <img src="img/logo/php.png" alt="php" Title="Php 7">
                                <img src="img/logo/wamp.png" alt="mamp" title="Wamp">
                                <img src="img/logo/mysql.png" alt="mysql" Title="MySQL">
                            </div>    
                            <div class="embed-responsive embed-responsive-16by9 mt-3">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2527.0529265127516!2d3.154989315647157!3d50.70040237720408!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d5855a3d4f65%3A0x892df9ce72cd44f0!2s3W%20Academy!5e0!3m2!1sfr!2sfr!4v1589145479160!5m2!1sfr!2sfr" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>        
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </main>

    <footer class="fixed-bottom text-center font-weight-bold"> <!-- FOOTER -->
       Tous Droits Réservés © 2020 - DESCAMPS Thibaut
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>   
</body>

</html>