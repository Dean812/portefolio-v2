<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portefolio de Thibaut Descamps</title>
    <!-- fonts -->
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- fontawesome - icone -->
    <script src="https://kit.fontawesome.com/27a05ab3d9.js" crossorigin="anonymous"></script>
    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <header class="bg-rdark">                 <!--  HEADER   -->
        <div class="header_top">
            <section>
                <h4 class=" greentext font-weight-bold">Thibaut Descamps</h4>
                <p class="greytext">Développeur web Back-end</p>   
            </section>
            <aside class="language">
                <p class="greentext font-weight-bold">Français</p>
                <p class="greytext font-weight-bold">English</p>
            </aside>
        </div>
        <nav class="bg-dark">
            <div class="header_down">
                <a href="index.php"><i class="fas fa-home"></i><p class="hide">Accueil</p></a>
            </div>
            <div class="header_down">
                <a href="skills.php"><i class="fas fa-tools"></i><p class="hide">Compétences</p></a>
            </div>
            <div class="header_down">
                <a href="projects.php"><i class="fas fa-folder-open actif"></i><p class="hide actif">Projets</p></a> 
            </div>
            <div class="header_down">
                <a href="timeline.php"><i class="fas fa-hourglass-start"></i><p class="hide">Chronologie</p></a>
            </div>
            <div class="header_down">
                <a href="presentation.php"><i class="fas fa-user-circle"></i><p class="hide">Présentation</p></a>
            </div>
            <div class="header_down">
                <a href="contact.php"><i class="fas fa-envelope"></i><p class="hide">Contact</p></a>
            </div>
        </nav>
    </header>
    <main id="projects">                  <!--      MAIN       -->
        <div class="title">
            <h2 class="font-weight-bold" id="#top">Projets</h2>
        </div> 
        <nav>
            <ul class="d-flex flex-row justify-content-around">
                <li><a href="#future" class="text-grey font-weight-bold">Futur</a></li>
                <li><a href="#present" class="text-grey font-weight-bold">Présent</a></li>
                <li><a href="#past" class="text-grey font-weight-bold">Passée</a></li>
            </ul>
        </nav>
        <div id="Future">
            <h3 class="text-danger ml-4">-- Futur --</h3>
            <div class="banner">
                <section>
                    <div id="carouselExampleControls-f1" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src=".\img\carousel\tew\tew_p1.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\tew\tew_p2.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\tew\tew_p3.JPG" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\tew\tew_p4.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\tew\tew_p5.JPG" alt="Second slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls-f1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls-f1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </section>
                <aside class="overflow-auto p-4">
                    <h4><u>Association des Tracteurs en Weppes</u></h4>
                    <p><b>Synopsis</b> : Ce projet est né du désir de mettre à jour un site pour 
                    une association locale.<br/> 
                    Ce site m'a permit de travailler sur le Framework Boostrap.   </p>
                    <p><b>Outils</b> :  PHP - CSS - Bootstrap - gitkraken  </p>
                    <p><b>Site d'origine</b>: <a href="http://tracteursenweppes.chez.com/">http://tracteursenweppes.chez.com/</a>  </p>
                    <p><b>Date</b>: 2020</p>
                </aside>
            </div>
            <div class="banner">
                <section>
                    <div id="carouselExampleControls-f2" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-1.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-2.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-3.JPG" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-4.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.1.JPG" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls-f2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls-f2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </section>
                <aside class="overflow-auto p-4">
                    <h4><u>Portefolio V1</u></h4>
                    <p><b>Synopsis</b> : Dans le but d'acquérir un certificat de fin de formation 
                    à la 3wschool, J'ai choisi le portefolio. Ce choix me permettait de mettre en avant de nombreuses connaissances vue en cours, d'en apporter de nouvelle et d'être visible sur internet.</p>
                    <p><b>Outils</b> :  PHP - CSS - Bootstrap - JS - MySql - gitkraken  </p>
                    <p><b>Environement</b>: Projet libre de fin de Formation</p>
                    <p><b>Date</b> : 2018</p>
                </aside>
            </div>
            <h3 class="text-danger mx-4" id="present">-- Présent --<span class="float-right"><a href="#top">^</a></span></h3>
            <div class="banner">
                <section>
                    <div id="carouselExampleControls-p" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-1.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-2.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-3.JPG" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-4.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.1.JPG" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls-p" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls-p" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </section>
                <aside class="overflow-auto p-4">
                    <h4><u>Portefolio V1</u></h4>
                    <p><b>Synopsis</b> : Dans le but d'acquérir un certificat de fin de formation 
                    à la 3wschool, J'ai choisi le portefolio. Ce choix me permettait de mettre en avant de nombreuses connaissances vue en cours, d'en apporter de nouvelle et d'être visible sur internet.</p>
                    <p><b>Outils</b> :  PHP - CSS - Bootstrap - JS - MySql - gitkraken  </p>
                    <p><b>Environement</b>: Projet libre de fin de Formation</p>
                    <p><b>Date</b> : 2018</p>
                </aside>
            </div>
            <h3 class="text-danger mx-4" id="past">-- Passée --  <span class="float-right"><a href="#top">^</a></span> </h3>
            <div class="banner">
            <section>
                    <div id="carouselExampleControls-p1" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-1.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-2.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-3.JPG" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-4.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.1.JPG" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls-p1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls-p1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </section>
                <aside class="overflow-auto p-4">
                    <h4><u>Portefolio V1</u></h4>
                    <p><b>Synopsis</b> : Dans le but d'acquérir un certificat de fin de formation 
                    à la 3wschool, J'ai choisi le portefolio. Ce choix me permettait de mettre en avant de nombreuses connaissances vue en cours, d'en apporter de nouvelle et d'être visible sur internet.</p>
                    <p><b>Outils</b> :  PHP - CSS - Bootstrap - JS - MySql - gitkraken  </p>
                    <p><b>Environement</b>: Projet libre de fin de Formation</p>
                    <p><b>Date</b> : 2018</p>
                </aside>
            </div>
            <div class="banner">
            <section>
                    <div id="carouselExampleControls-p2" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-1.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-2.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-3.JPG" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-4.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.1.JPG" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls-p2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls-p2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </section>
                <aside class="overflow-auto p-4">
                    <h4><u>Portefolio V1</u></h4>
                    <p><b>Synopsis</b> : Dans le but d'acquérir un certificat de fin de formation 
                    à la 3wschool, J'ai choisi le portefolio. Ce choix me permettait de mettre en avant de nombreuses connaissances vue en cours, d'en apporter de nouvelle et d'être visible sur internet.</p>
                    <p><b>Outils</b> :  PHP - CSS - Bootstrap - JS - MySql - gitkraken  </p>
                    <p><b>Environement</b>: Projet libre de fin de Formation</p>
                    <p><b>Date</b> : 2018</p>
                </aside>
            </div>
            <div class="banner">
            <section>
                    <div id="carouselExampleControls-p3" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-1.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-2.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-3.JPG" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-4.JPG" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.JPG" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src=".\img\carousel\portefolio\pf_p-5.1.JPG" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls-p3" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls-p3" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </section>
                <aside class="overflow-auto p-4">
                    <h4><u>Portefolio V1</u></h4>
                    <p><b>Synopsis</b> : Dans le but d'acquérir un certificat de fin de formation 
                    à la 3wschool, J'ai choisi le portefolio. Ce choix me permettait de mettre en avant de nombreuses connaissances vue en cours, d'en apporter de nouvelle et d'être visible sur internet.</p>
                    <p><b>Outils</b> :  PHP - CSS - Bootstrap - JS - MySql - gitkraken  </p>
                    <p><b>Environement</b>: Projet libre de fin de Formation</p>
                    <p><b>Date</b> : 2018</p>
                </aside>
            </div>
        </div>
       
    </main>

    <footer class="bg-transparent fixed-bottom text-center font-weight-bold"> <!-- FOOTER -->
       Tous Droits Réservés © 2020 - DESCAMPS Thibaut
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>   
    
</body>

</html>